function getRequest(){

    req = new XMLHttpRequest;
    req.onreadystatechange = function (){

        if( this.readyState == 4 && this.status == 200){
            
            document.getElementById('demo').innerHTML = this.responseText;

        }

    };

    req.open('GET', 'index.php?firstParameter=First%20Name', true);
    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    req.send();

}

//getRequest();

function postRequest(){
    var myArray = ['first', 'second', 'third'];

    req = new XMLHttpRequest;
    req.onreadystatechange = function (){

        if( this.readyState == 4 && this.status == 200){
            
            document.getElementById('demo').innerHTML = this.responseText;

        }

    };

  //Serialize the data
  var queryString = "";
  for(var i = 0; i < myArray.length; i++) {
    queryString += i + "=" + myArray[i];

        //Append an & except after the last element
        if(i < myArray.length - 1) {
         queryString += "&";
     }
 }

 console.log(queryString);
 req.open('POST', 'index.php', true);
 req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
 req.send(queryString);

}

postRequest();